﻿using UnityEditor;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

namespace Game.editor
{
    [CustomEditor(typeof(LevelController))]
    public class LevelControllerEditor : Editor
    {
        private LevelController controller;

        string labelText = "Level Editor: ";

        string label;

        const string PATH_FOR_LEVELS = "Assets/Resources/Levels";

        const string LEVEL_BASE_NAME = "GameLevel";

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();

            label = labelText + " " + Selection.activeObject;
            
            EditorGUILayout.LabelField(label);
            EditorGUILayout.Space();

            if (GUILayout.Button("Load Level"))
            {
                Level level = Selection.activeObject as Level;

                if (level)
                {
                    ClearBoard();
                    InstatiateLevel(level);
                }
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Save Level"))
            {
                ScriptableObjectUtility.SaveAsset(GetLevel());
            }

            EditorGUILayout.Space();

            if (GUILayout.Button("Create new level"))
            {
                int number = GetNextLevelNumber();

                ScriptableObjectUtility.CreateNewAsset(PATH_FOR_LEVELS, LEVEL_BASE_NAME + (++number), GetLevel());
            }

            EditorGUILayout.EndVertical();
        }

        private int GetNextLevelNumber()
        {
            var guids = AssetDatabase.FindAssets("t:Level");
            var names = new List<string>();

            foreach (var g in guids)
            {
                names.Add(AssetDatabase.GUIDToAssetPath(g));
            }

            var str = names.Last();
            var numbers = str.Where(c => char.IsDigit(c));
            string numberStrLastLevel = "";

            foreach (var n in numbers)
            {
                numberStrLastLevel += n.ToString();
            }

            int number = 0;
            var result = int.TryParse(numberStrLastLevel, out number);

            return number;
        }


        private void ClearBoard()
        {
            int count = controller.LevelParent.childCount;

            while (controller.LevelParent.childCount > 0)
            {
                DestroyImmediate(controller.LevelParent.GetChild(count - 1).gameObject);
                count = controller.LevelParent.childCount;
            }
        }

        private void InstatiateLevel(Level level)
        {
            GameObject enemyPrefab = Resources.Load<GameObject>("Prefabs/"
                                              + level.EnemyPrefabName);

            GameObject goalPrefab = Resources.Load<GameObject>("Prefabs/"
                                               + level.GoalPrefabName);

            for (int i = 0; i < level.EnemyPositions.Length; i++)
            {
                var enemy = Instantiate(enemyPrefab, level.EnemyPositions[i], Quaternion.identity);

                enemy.transform.SetParent(controller.LevelParent);
            }

            var goal = Instantiate(goalPrefab, level.GoalPosition, Quaternion.identity);

            goal.transform.SetParent(controller.LevelParent);

           // controller.GameTable.SetEdgeColliderPoints(goal);
        }

        private Level GetLevel()
        {
            Level level = ScriptableObject.CreateInstance<Level>();

            var enemies = controller.LevelParent.GetComponentsInChildren<Transform>()
                         .Where(t => t.CompareTag("Enemy")).ToArray();

            level.EnemyPositions = new Vector2[enemies.Count()];
            
            for (int i = 0; i < enemies.Count(); i++)
            {
                level.EnemyPositions[i] = enemies[i].position;
            }

            level.GoalPosition = GameObject.FindGameObjectWithTag("Finish").transform.position;

            return level;
        }


        private void OnEnable()
        {
            controller = (LevelController)target;
        }
    }
}