﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTable : MonoBehaviour
{

    private EdgeCollider2D edgeCollider2D;

    void Awake()
    {
        edgeCollider2D = GetComponent<EdgeCollider2D>();
    }

    /// <summary>
    /// Set points to EdgeCollider
    /// </summary>
    /// <param name="goal">GameObject Goal</param>
    public void SetEdgeColliderPoints(GameObject goal)
    {
        var size = GetGoalSize(goal);

        var objScale = edgeCollider2D.transform.localScale.x;

        var points = edgeCollider2D.points;

        points[0] = new Vector2(size.x / objScale, edgeCollider2D.points[0].y);

        int endPointIndex = edgeCollider2D.points.Length - 1;

        points[endPointIndex] = new Vector2(size.y / objScale,
                                            edgeCollider2D.points[endPointIndex].y);

        edgeCollider2D.points = points;
    }

    /// <summary>
    /// Calculate goal size
    /// </summary>
    /// <param name="goal">GameObject Goal</param>
    /// <returns>Vector2 size, y = right side X</returns>
    private Vector2 GetGoalSize(GameObject goal)
    {
        float x = goal.transform.position.x;

        float gloatXsize = goal.transform.localScale.x / 2;

        float startPointPosX = x - gloatXsize;

        float endPointPosX = x + gloatXsize;

        return new Vector2(startPointPosX, endPointPosX);
    }
}
