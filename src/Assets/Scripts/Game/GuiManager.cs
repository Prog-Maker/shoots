﻿using Common;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class GuiManager : MonoBehaviour, MMEventListener<MMGameEvent>
    {
#pragma warning disable 0649
        [SerializeField]
        private Text scoreTextBox;
#pragma warning restore 0649


        public void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.EventName == "NewLevel")
            {
                if (scoreTextBox)
                {
                    int level = Convert.ToInt32(eventType.Arg);
                    scoreTextBox.text = (++level).ToString();
                }
            }
        }

        private void OnEnable()
        {
            this.MMEventStartListening<MMGameEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<MMGameEvent>();
        }
    }
}
