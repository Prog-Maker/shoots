﻿using Common;
using System.Collections;
using UnityEngine;

namespace Game
{
    public class LevelController : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        [SerializeField]
        int levelNumber = 0;

#pragma warning disable 0649
        [SerializeField]
        private Transform levelParent;

        [SerializeField]
        private GameObject player;

        [SerializeField]
        private Vector2 startPlayerPosition;

        [SerializeField]
        private GameTable gameTable;
#pragma warning restore 0649

        private Level currentLevel;

        private const string LEVEL_LOSE = "LevelLose";
        private const string LEVEL_COMPLETE = "LevelComplete";
        private const string LEVEl_START = "LevelStart";
        private const string NEW_LEVEL = "NewLevel";

        public Transform LevelParent => levelParent;

        public GameTable GameTable => gameTable;


        void Start()
        {
            currentLevel = LoadLevel(levelNumber);

            if (!currentLevel) 
            { 
                Debug.Log("Load level Error");
                return;
            }

            CreateLevel();
        }

        private Level LoadLevel(int number)
        {
            return Resources.Load<Level>("Levels/GameLevel" + number);
        }

        private void ClearLevelObjects()
        {
            for (int i = 0; i < LevelParent.childCount; i++)
            {
                Destroy(LevelParent.GetChild(i).gameObject);
            }
        }

        private void CreateLevel()
        {
            MMGameEvent.Trigger(NEW_LEVEL, levelNumber);

            StopAllCoroutines();
            
            ReturnPlayerToStartPositiion();
            
            ClearLevelObjects();

            GameObject enemyPrefab = Resources.Load<GameObject>("Prefabs/"
                                               + currentLevel.EnemyPrefabName);
            
            GameObject goalPrefab = Resources.Load<GameObject>("Prefabs/"
                                               + currentLevel.GoalPrefabName);

            InstatiateLevelObjects(currentLevel, enemyPrefab, goalPrefab);
        }

        private void ReturnPlayerToStartPositiion()
        {
            if (!player) Debug.LogWarning("Player is NULL");

            player.GetComponent<PlayerSwipeController>().ResetPlayer(startPlayerPosition);
        }

        private void InstatiateLevelObjects(Level level, GameObject enemyPrefab, GameObject goalPrefab)
        {
            for (int i = 0; i < level.EnemyPositions.Length; i++)
            {
                var enemy = Instantiate(enemyPrefab, level.EnemyPositions[i], Quaternion.identity);

                enemy.transform.SetParent(LevelParent);
            }

            var goal = Instantiate(goalPrefab, level.GoalPosition, Quaternion.identity);

            goal.transform.SetParent(LevelParent);

            if (!gameTable) Debug.LogWarning("GameData is NUll");
            
            gameTable?.SetEdgeColliderPoints(goal);
        }

        private void ReloadLevel()
        {
            CreateLevel();
        }

        private void LoadNextLevel()
        {
            var level = LoadLevel(++levelNumber);
            
            if (level != null)
            {
                currentLevel = level;
                MMGameEvent.Trigger("CurrentLevelComplete", levelNumber);
            }
            else
            {
                levelNumber = 0;
                currentLevel = LoadLevel(levelNumber);

                if (currentLevel == null)
                {
                    Debug.Log("Fatal error - Do not load levels");
                }
            }

            StopAllCoroutines();
            StartCoroutine(GoToNextLevel());
        }

        private IEnumerator GoToNextLevel()
        {
            yield return new WaitForSeconds(1);
            CreateLevel();
        }


        public void OnMMEvent(MMGameEvent eventType)
        {
            switch (eventType.EventName)
            {
                case LEVEL_COMPLETE:
                    LoadNextLevel();
                    break;
                case LEVEL_LOSE:
                    ReloadLevel();
                    break;
                case LEVEl_START:
                    StartCoroutine(StartLevel());
                    break;
            }
        }

        private IEnumerator StartLevel()
        {
            yield return new WaitForSeconds(8);
            ReloadLevel();
        }

        private void OnEnable()
        {
            this.MMEventStartListening<MMGameEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<MMGameEvent>();
        }
    }
}
