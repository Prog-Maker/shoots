﻿using UnityEngine;
using Common;

namespace Game
{
    public class LevelEvent : MonoBehaviour
    {
        enum CollisionType
        {
            TriggerEnter2D, CollisionEnter2D
        }

        [SerializeField]
        private string eventName = "";

        [SerializeField]
        private CollisionType collisionType = CollisionType.TriggerEnter2D;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collisionType == CollisionType.TriggerEnter2D)
            {
                if (collision.CompareTag("Player"))
                {
                    InvokeIvent();
                }
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collisionType == CollisionType.CollisionEnter2D)
            {
                if (collision.collider.CompareTag("Player"))
                {
                    InvokeIvent();
                }
            }
        }

        private void InvokeIvent()
        {
            MMGameEvent.Trigger(eventName);
        }
    }
}
