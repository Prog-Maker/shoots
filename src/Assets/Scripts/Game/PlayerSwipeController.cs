﻿using Common;
using UnityEngine;

namespace Game
{
    public class PlayerSwipeController : MonoBehaviour
    {
        [SerializeField]
        private float minSpeed = 10f, maxSpeed = 40f;

        private Camera cam;

        private Rigidbody2D rb2d;

        private Vector2 startTouchPosition = Vector2.zero;
        
        private Vector2 endTouchPosition = Vector2.zero;

        private bool isLevelStarted = false;

        void Awake()
        {
            rb2d = GetComponent<Rigidbody2D>();
            
            cam = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            if (!isLevelStarted)
            {
                MoveDirection();
            }
        }

        private void MoveDirection()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    startTouchPosition = cam.ScreenToWorldPoint(touch.position);
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    endTouchPosition = cam.ScreenToWorldPoint(touch.position);

                    var direction = endTouchPosition - startTouchPosition;
                    
                    var speed = Mathf.Abs(touch.deltaPosition.magnitude);

                    speed = Mathf.Clamp(speed, minSpeed, maxSpeed);

                    SetInmpulse(direction, speed);
                }
            }
        }


        private void SetInmpulse(Vector2 direction, float speed)
        {
            if (direction != Vector2.zero)
            {
                RotateToDirection(direction);

                rb2d.AddForce(transform.right * speed, ForceMode2D.Impulse);

                isLevelStarted = true;

                MMGameEvent.Trigger("LevelStart");
            }
        }

        private void RotateToDirection(Vector2 direction)
        {
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            Quaternion rot = Quaternion.AngleAxis(angle, Vector3.forward);

            transform.rotation = rot;
        }

        private void RestartLevel()
        {
            isLevelStarted = false;
        }

        public void ResetPlayer(Vector2 startPos)
        {
            rb2d.Sleep();
            transform.position = startPos;
            rb2d.velocity = Vector2.zero;

            RestartLevel();
        }
    }
}
