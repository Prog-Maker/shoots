﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Game.editor
{
    public static class ScriptableObjectUtility
    {
        /// <summary>
        //  This makes it easy to create, name and place unique new ScriptableObject asset files.
        /// </summary>
        public static void CreateNewAsset<T>(string path, string name = "", T assetparam = null) where T : ScriptableObject
        {
            T asset = assetparam ?? ScriptableObject.CreateInstance<T>();

            //string path = "Assets/Resources";

            //string path = AssetDatabase.GetAssetPath(Selection.activeObject);

            //if (path == "")
            //{
            //    path = "Assets/Resources";
            //}
            //else if (Path.GetExtension(path) != "")
            //{
            //    path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            //}

            name = name == "" ? typeof(T).ToString() : name;

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();

            AssetDatabase.Refresh();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }

        public static void SaveAsset(Object asset)
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);

            Selection.activeObject = asset;

            AssetDatabase.CreateAsset(Selection.activeObject, path);

            AssetDatabase.SaveAssets();

            AssetDatabase.Refresh();
        }
    }
}