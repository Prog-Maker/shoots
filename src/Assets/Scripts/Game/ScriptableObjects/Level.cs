﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Level : ScriptableObject
    {
        public string EnemyPrefabName = "Enemy";

        public string GoalPrefabName = "Goal";
        
        public Vector2[] EnemyPositions;

        public Vector2 GoalPosition;
    }
}
