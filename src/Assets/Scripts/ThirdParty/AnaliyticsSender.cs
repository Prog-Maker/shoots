﻿using Common;
using GameAnalyticsSDK;
using System;
using UnityEngine;

namespace Game.Analytics
{
    public class AnaliyticsSender : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        void Start()
        {
            GameAnalytics.Initialize();
        }

        public void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.EventName == "CurrentLevelComplete")
            {
                int level = Convert.ToInt32(eventType.Arg);
                GameAnalytics.NewDesignEvent("CurrentLevelComplete", level);
            }
        }

        private void OnEnable()
        {
            this.MMEventStartListening<MMGameEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<MMGameEvent>();
        }
    }
}
